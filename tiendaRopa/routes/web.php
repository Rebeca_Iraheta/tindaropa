<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProducDisp@index',function(){
	return view('productos');
});

Route::get('/Factura', 'ProducDisp@factura', function(){
	return view('factura');
});

Route::get('/Detalle/{id}', 'ProducDisp@detalles', function(){
	return view('detalle');
});

Route::get('/search','ProducDisp@search')->name('posts.search');

Route::get('/crear','ProducDisp@create',function(){
	return view('createProducto');
});

Route::post('/ingresar','ProducDisp@store',function(){
	return view('/');
});

Route::get('eliminar{id}', 'ProducDisp@eliminar')->where('id','[0-9]+')->name('eliminar');

Route::get('eliminarF{id}', 'ProducDisp@eliminarF')->where('id','[0-9]+')->name('eliminarF');

Route::get('eliminarD{id}', 'ProducDisp@eliminarD')->where('id','[0-9]+')->name('eliminarD');

Route::get('/Factura','ProducDisp@createFactura', function(){
	return view('factura');
});

Route::post('/ingresarFactura','ProducDisp@storeFactura',function(){
	return view('Factura');
});

Route::post('/ingresarDetalle','ProducDisp@storeDetalle',function(){
	return view('detalle');
});

Route::get('/editar/{id}', 'ProducDisp@edit')->name('editar');

Route::post('update/{id}','ProducDisp@update')->name('update');