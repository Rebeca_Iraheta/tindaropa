<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model
{
    public $timestamps = false;
    protected $table = 'detalle';
    protected $primarykey = 'id';
    protected $fillable = ['id','idFactura','idProducto','cantidad','precio','total'];
}
