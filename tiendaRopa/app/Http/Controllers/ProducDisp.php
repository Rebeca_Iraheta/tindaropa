<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Procedures\productoProcedure;
use Illuminate\Http\Request;
use App\Http\Requests\ItemUpdateRequest;

class ProducDisp extends Controller
{
	protected $productoProcedure;

	public function __construct(productoProcedure $productoProcedure){
		$this->productoProcedure = $productoProcedure;
	}

     /*fn: Mostrar vista de productos
    @param: na 
    @return: */
    public function index(Request $request){
    	$productoProcedure = $this->productoProcedure->getProducto();
    	return view('productos', compact(['productoProcedure']));
    }

    /*fn: Mostrar vista del formulario de ingreso de productos
    @param: na 
    @return: vista del formulario de create*/

    public function create(){
       $color = $this->productoProcedure->getColor();
       $talla = $this->productoProcedure->getTalla();
       $genero = $this->productoProcedure->getGenero();
       $categoria = $this->productoProcedure->getCategoria();
       $marca = $this->productoProcedure->getMarca();
    	return view('createProducto', compact('color','talla', 'genero', 'categoria','marca'));
    }

    //hace el ingreso de un producto a la tabla productos.
    public function store(Request $request){
        $array = [$request->nombre, $request->idColor, $request->idTalla, $request->precio, $request->idGenero,$request->idCategoria, $request->idMarca, $request->cantidad];
        $producto = $this->productoProcedure->ingresarPro($array);
        
        return redirect('/');
    }

    public function edit($id){
      $productos = producto::find($id);
      $color = $this->productoProcedure->getColor();
      $talla = $this->productoProcedure->getTalla();
      $genero = $this->productoProcedure->getGenero();
      $categoria = $this->productoProcedure->getCategoria();
      $marca = $this->productoProcedure->getMarca();
      return view('actualizarProduc',['producto'=>$productos,'color'=>$color,'talla'=>$talla,'genero'=>$genero,'categoria'=>$categoria,'marca'=>$marca]);
    }

    public function update(ItemUpdateRequest $request, $id){
        $productos = Producto::find($id);
        $productos->nombre = $request->nombre;
        $productos->idColor = $request->idColor;
        $productos->idTalla = $request->idTalla;
        $productos->precio = $request->precio;
        $productos->idGenero = $request->idGenero;
        $productos->idCategoria = $request->idCategoria;
        $productos->idMarca = $request->idMarca;
        $productos->cantidad = $request->cantidad;

        //Actualizo los datos en la tabla de productos
        $productos->save();
        return redirect('/');
    }

    public function detalle(){
        $factura = $this->productoProcedure->getFacturas();
        $producto = $this->productoProcedure->getProductos();
        $detalle = $this->productoProcedure->getDetalle();
        return view('detalle', compact('factura','producto','detalle'));
    }

    public function storeDetalle(Request $request){
        $array = [$request->idFactura,$request->idProducto,$request->cantidad,$request->precio];
        $detalle = $this->productoProcedure->ingresarDetalle($array);
        return redirect('Detalle');
    }

    public function eliminar($id){
        \DB::table('producto')->where('idProducto', '=', $id)->delete();
        return redirect('/');
    }

    public function eliminarF($id){
        \DB::table('factura')->where('idFactura', '=', $id)->delete();
        return redirect('Factura');
    }

    public function eliminarD($id){
        \DB::table('detalle')->where('idDetalle', '=', $id)->delete();
        return redirect('Detalle');
    }

    public function createFactura(){
        $modoPago = $this->productoProcedure->getModoPago();
        $vendedor = $this->productoProcedure->getVendedor();
        $sucursal = $this->productoProcedure->getSucursal();
        $facturaP = $this->productoProcedure->getFactura();
        return view('factura', compact('modoPago','vendedor','sucursal','facturaP'));
    }

    public function storeFactura(Request $request){
        $array = [$request->fecha,$request->idModoPago,$request->cliente,$request->idVendedor,$request->idSucursal];
        $factura = $this->productoProcedure->ingresarFactura($array);
        return redirect('Factura');
    }
}