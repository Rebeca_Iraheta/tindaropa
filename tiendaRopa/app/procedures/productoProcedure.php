<?php 

namespace App\Procedures;

class productoProcedure
{
/*	fn: Devuelve un listado de paConsultarProductos
	@param: na 
	@return: Ejecucion de procedimientos almacenados*/
	public function getProducto(){
		return \DB::select('CALL paConsultarProductos');
	}

	public function getFactura(){
		return \DB::select('CALL paConsultarFacturas');
	}

	public function ingresarPro($array){
		return \DB::select('CALL paIngresarProducto(?,?,?,?,?,?,?,?)', $array);
	}

	public function ingresarDetalle($array){
		return \DB::select('CALL paIngresarDetalle(?,?,?,?)', $array);
	}

	public function getColor(){
		return \DB::select('CALL pa_consultarColor');
	}

	public function getTalla(){
		return \DB::select('CALL paConsultarTalla');
	}

	public function getGenero(){
		return \DB::select('CALL paConsultarGenero');
	}

	public function getCategoria(){
		return \DB::select('CALL paConsultarCategoria');
	}

	public function getMarca(){
		return \DB::select('CALL paConsultarMarca');
	}

	public function ingresarFactura($array){
		return \DB::select('CALL paIngresarFactura(?,?,?,?,?)',$array);
	}

	public function getModoPago(){
		return \DB::select('CALL paConsultarModoPago');
	}

	public function getVendedor(){
		return \DB::select('CALL paConsultarVendedor');
	}

	public function getSucursal(){
		return \DB::select('CALL paConsultarSucursal');
	}

	public function getFacturas(){
		return \DB::select('CALL paConsultarFactura');
	}

	public function getProductos(){
		return \DB::select('CALL paConsultarProducto');
	}

	public function getDetalle(){
		return \DB::select('CALL paConsultarDetalles');
	}
}