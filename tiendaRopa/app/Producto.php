<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
	public $timestamps = false;
    protected $table = 'producto';
    protected $primarykey = 'id';
    protected $fillable = ['id','nombre','idColor','idTalla','precio','idGenero','idMarca'];
                 

   
}
