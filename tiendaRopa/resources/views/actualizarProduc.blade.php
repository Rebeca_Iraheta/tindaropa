<!DOCTYPE html>
<html>	
<head>	
<title></title>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
</head>
<style>
    body {
      background-color: #B0E0E6;
    }
  </style>
<body>

<div class="container-fluid">
		<h2 class="text-center font-weight-bold font-italic text-primary">Actualizar Productos</h2>
		</div>
<div class="container">
     <form action="{{ route('update', $producto->id)}}" role="form" enctype="multipart/form-data" method="post">
     	
     	<input type="hidden" name="_token" value="{{ csrf_token() }}">
         <table class="table">
	        <tr>
					<td><label for="exampleFormControlInput1">Nombre: </label></td>
					<input type="hidden" name="idProducto" value="{{$producto->idProducto}}">
					<td><input class="form-control" type="text" name="nombre" value="{{$producto->nombre}}" ></td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Color:</label></td>
					<td>
						<select class="form-control" name="idColor" value="idColor">
							<option value="">--Seleccione--</option>
							@foreach($color as $c)
							<option value="{{ $c->idColor }}" <?php if($c->idColor==$producto->idColor){echo "selected"; } ?>>{{$c->nombreColor}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Talla:</label> </td>
					<td>
						<select class="form-control" name="idTalla" value="idTalla">
							<option value="">--Seleccione--</option>
							@foreach($talla as $t)
							<option value="{{ $t->idTalla }}" <?php if($t->idTalla==$producto->idTalla){echo "selected"; } ?>>{{$t->nombreTalla}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Precio:</label> </td>
					<td><input class="form-control" type="number" step="0.01"  name="precio" value="{{$producto->precio}}"></td>
				</tr>
				
				<tr>
					<td><label for="exampleFormControlInput1">Genero de la ropa: </label></td>
					<td>
						<select class="form-control" name="idGenero" value="idGenero">
							<option value="">--Seleccione--</option>
							@foreach($genero as $g)
							<option value="{{$g->idGenero}}" <?php if($g->idGenero==$producto->idGenero){ echo "selected"; } ?>>{{$g->nombreGenero}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Nombre de la categoria: </label></td>
					<td>
						<select class="form-control" name="idCategoria" value="idCategoria">
							<option value="">--Seleccione--</option>
							@foreach($categoria as $ca)
							<option value="{{$ca->idCategoria}}" <?php if($ca->idCategoria==$producto->idCategoria){echo "selected"; } ?>>{{$ca->nombreCategoria}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Marca:</label></td>
					<td>
						<select class="form-control" name="idMarca" value="idMarca">
							<option value="">--Seleccione--</option>
							@foreach($marca as $m)
							<option value="{{$m->idMarca}}" <?php if($m->idMarca==$producto->idMarca){echo "selected"; } ?>>{{$m->nombreMarca}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Cantidad: </label></td>
					<td><input class="form-control" type="number" name="cantidad" value="{{$producto->cantidad}}"></td>
				</tr>
				<tr>
					<td><a href="/" class="btn btn-primary" role="button" aria-pressed="true">Cancelar</a></td>
					<td><input class="btn btn-primary" type="submit" value="Guardar"></td>
					
				</tr>
			</table>
				</form>
</div>
	</body>
</html>