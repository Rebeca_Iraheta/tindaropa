<!DOCTYPE html>
<html>
	<head>
		<title>Productos entrantes</title>

		<!-- CSRF TOKEN -->
		
		
		<!-- Scripts -->
		<script src="{{ asset('js/app.js')}}"></script>

		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
    	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    	<!-- styles -->
    	<link rel="stylesheet" href="{{ asset('css/app.css')}}" rel="stylesheet">
	</head>
	<style>
    body {
      background-color: #B0E0E6;
    }
  </style>
	<body>
		<div class="container">
		<div class="container-fluid">
		<h2 class="text-center font-weight-bold font-italic text-primary">Ingresar nuevos productos</h2>
		</div>
		<br><br><br>
		<form method="POST" action="ingresar" class="form-inline">
			@csrf
			
			<div class="container">
			<table class="table">
				<tr>
					<td><label for="exampleFormControlInput1">Nombre: </label></td>
					<td><input class="form-control" type="text" name="nombre" ></td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Color:</label></td>
					<td>
						<select class="form-control" name="idColor" value="idColor">
							<option value="">--Seleccione--</option>
							@foreach($color as $c)
							<option value="{{ $c->idColor }}">{{$c->nombreColor}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Talla:</label> </td>
					<td>
						<select class="form-control" name="idTalla" value="idTalla">
							<option value="">--Seleccione--</option>
							@foreach($talla as $t)
							<option value="{{ $t->idTalla }}">{{$t->nombreTalla}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Precio:</label> </td>
					<td><input class="form-control" type="number" step="0.01" value="precio" name="precio"></td>
				</tr>
				
				<tr>
					<td><label for="exampleFormControlInput1">Genero de la ropa: </label></td>
					<td>
						<select class="form-control" name="idGenero" value="idGenero">
							<option value="">--Seleccione--</option>
							@foreach($genero as $g)
							<option value="{{$g->idGenero}}">{{$g->nombreGenero}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Nombre de la categoria: </label></td>
					<td>
						<select class="form-control" name="idCategoria" value="idCategoria">
							<option value="">--Seleccione--</option>
							@foreach($categoria as $ca)
							<option value="{{$ca->idCategoria}}">{{$ca->nombreCategoria}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Marca:</label></td>
					<td>
						<select class="form-control" name="idMarca" value="idMarca">
							<option value="">--Seleccione--</option>
							@foreach($marca as $m)
							<option value="{{$m->idMarca}}">{{$m->nombreMarca}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="exampleFormControlInput1">Cantidad: </label></td>
					<td><input class="form-control" type="number" name="cantidad"></td>
				</tr>
				<tr>
					<td><a href="/" class="btn btn-primary" role="button" aria-pressed="true">Cancelar</a></td>
					<td><input class="btn btn-primary" type="submit" value="Guardar"></td>
					
				</tr>
			</table>
			</div>
		</form>
		</div>
	</body>
</html>