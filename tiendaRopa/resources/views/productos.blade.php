<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
 		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
	</head>
	<style>
    body {
      background-color: #B0E0E6;
    }
  </style>
	<body>
		<div class="container-fluid">
			<nav class="navbar" style="background-color: #008b8b; " >
		<ul class="nav nav-tabs">
  
  <li class="nav-item">
    <a style="color: black" class="nav-link" href="/"><b>Lista de productos</b></a>
  </li>
  <li class="nav-item">
    <a style="color: black" class="nav-link" href="Factura"><b>Factura</b></a>
  </li>
  <li class="nav-item">
    <a style="color: black" class="nav-link " href="Detalle"><b>Detalles</b></a>
  </li>
</ul>
</nav>
</div>
		<div class="container-fluid">
		<h2 class="text-center font-weight-bold font-italic text-primary">Productos disponibles</h2>
		</div>

		<div class="container-fluid">
		<a href="crear" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Ingresar un nuevo producto</a>
		</div>
		<br>
		<div class="container-fluid">
			<table id="example" border="1" class="table table-hover">
				<thead class="thead-dark">
					<tr>
						
						<th>Nombre</th>
						<th>Color</th>
						<th>Talla</th>
						<th>Precio</th>
						<th>Genero</th>
						<th>Categoria</th>
						<th>Marca</th>
						<th>Cantidad</th>
						<th>Opcion</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($productoProcedure as $p) { ?>
						<tr>
							
							<td><?= $p->nombre ?></td>
							<td><?= $p->nombreColor ?></td>
							<td><?= $p->nombreTalla ?></td>
							<td><?= $p->precio ?></td>
							<td><?= $p->nombreGenero ?></td>
							<td><?= $p->nombreCategoria ?></td>
							<td><?= $p->nombreMarca ?></td>
							<td><?= $p->cantidad ?></td>

							<td><a href="{{Route('eliminar', $p->id)}}" class="btn btn-danger" role="button" onsubmit="return confirmarEliminar()" aria-pressed="true">Eliminar</a>
							<a class="btn btn-primary" href="{{Route('editar', $p->id)}}">Actualizar</a>
						</td>							
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Scripts -->
	    <script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
	    
	    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>  
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>	    
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );  
</script>

<script>
	function confirmarEliminar()
	{
		var x = confirm("¿Estas seguro que deseas eliminar el producto?");
		if (x) {
			return true;
		}else{
			return false;
		}
	}			
</script>
	</body>
	
</html>