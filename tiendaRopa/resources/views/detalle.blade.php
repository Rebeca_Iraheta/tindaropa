<html>
	<head>
		<title>detalle</title>
		<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	</head>
  <style>
    body {
      background-color: #B0E0E6;
    }

    .abs-center {
      display: flex;
      align-items: center;
      justify-content: center;
      min-height: 60vh;

    }
    form{
      width: 450;

    }
  </style>
	<body >
    <div class="container-fluid">
      <nav class="navbar" style="background-color: #008b8b; " >
    <ul class="nav nav-tabs">
  
  <li class="nav-item">
    <a style="color: black" class="nav-link" href="/"><b>Lista de productos</b></a>
  </li>
  <li class="nav-item">
    <a style="color: black" class="nav-link" href="Factura"><b>Factura</b></a>
  </li>
  <li class="nav-item">
    <a style="color: black" class="nav-link " href="Detalle"><b>Detalles</b></a>
  </li>
</ul>
</nav>
</div>
<div class="container-fluid">
	<h2 class="text-center font-weight-bold font-italic text-primary">Detalle de venta</h2>
</div>

<div class="abs-center">
<form method="POST" action="ingresarDetalle">
   @csrf
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label">N° Factura</label>
    <div class="col-sm-10">
      <select class="form-control" name="idFactura" id="">
        <option value="">--Seleccione--</option>
        @foreach($factura as $f)
        <option value="{{ $f->idFactura }}">{{ $f->idFactura}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label">Producto</label>
    <div class="col-sm-10">
      <select class="form-control" name="idProducto" id="">
        <option value="">--Seleccione--</option>
        @foreach($producto as $p)
        <option value="{{ $p->id}}">{{$p->nombre}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label  class="col-sm-2 col-form-label">Cantidad</label>
    <div class="col-sm-10">
      <input type="number" name="cantidad" class="form-control">
    </div>
  </div>
 <div class="form-group row">
    <label  class="col-sm-2 col-form-label">Precio</label>
    <div class="col-sm-10">
      <input type="numer" name="precio" class="form-control">
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-10">
    	<button type="submit" class="btn btn-primary">Guardar</button>
    </div>
  </div>
</form>
</div>

<div class="container-fluid">
  <table border="1" id="example" class="table table-hover data-table">
    <thead class="thead-dark">
      <tr>
        <th>Numero de factura</th>
        <th>Producto</th>
        <th>Cantidad</th>
        <th>Precio</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($detalle as $d)
      <tr>
        <td>{{$d->idFactura}}</td>
        <td>{{$d->nombre}}</td>
        <td>{{$d->cantidad}}</td>
        <td>{{$d->precio}}</td>
        <td><button type="submit" class="btn btn-primary">Imprimir</button>
          <a href="{{Route('eliminarD', $d->idDetalle)}}" class="btn btn-danger" role="button" aria-pressed="true">Eliminar</a></td></td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
      
      <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>  
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );  
</script>
	</body>
</html>