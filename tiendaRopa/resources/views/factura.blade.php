<html>
	<head>
		<title>Factura</title>
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
	</head>
  <style>
    body {
      background-color: #B0E0E6;
    }
    .abs-center {
      display: flex;
      align-items: center;
      justify-content: center;
      min-height: 60vh;

    }
    form{
      width: 450;

    }
  </style>
	<body>
    <div class="container-fluid">
      <nav class="navbar" style="background-color: #008b8b; " >
    <ul class="nav nav-tabs">
  
  <li class="nav-item">
    <a style="color: black" class="nav-link" href="/"><b>Lista de productos</b></a>
  </li>
  <li class="nav-item">
    <a style="color: black" class="nav-link" href="Factura"><b>Factura</b></a>
  </li>
  <li class="nav-item">
    <a style="color: black" class="nav-link " href="Detalle"><b>Detalles</b></a>
  </li>
</ul>
</nav>
</div>
  <div class="container-fluid">
    <h2 class="text-center font-weight-bold font-italic text-primary"> Factura</h2>
    </div>

<div class="abs-center">
<form method="POST" action="ingresarFactura">
  @csrf
  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Fecha</label>
    <div class="col-sm-10">
      <input type="date" class="form-control" name="fecha"  value="<?php echo Date('Y-m-d') ?>" readonly>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Tipo de pago</label>
    <div class="col-sm-10">
      <select name="idModoPago" class="form-control" value="idModoPago">
        <option value="">--Seleccione--</option>
        @foreach($modoPago as $m)
        <option value="{{ $m->idModoPago }}">{{$m->descripcion}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Cliente</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="cliente" placeholder="Digite nombre del cliente">
    </div>
  </div>
 <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Vendedor</label>
    <div class="col-sm-10">
      <select name="idVendedor" class="form-control" id="">
      	<option value="">--Seleccione--</option>
        @foreach($vendedor as $v)
        <option value="{{$v->idUsuario }}">{{$v->nombre}}</option>
        @endforeach
      </select>
    </div>
  </div>
   <div class="form-group row">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Sucursal</label>
    <div class="col-sm-10">
      <select name="idSucursal" class="form-control" id="">
        <option value="">--Seleccione--</option>
        @foreach($sucursal as $s)
        <option value="{{$s->idSucursal }}">{{$s->nombreSucursal}}</option>
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-10">
    	<button type="submit" class="btn btn-primary">Guardar</button>
      
      
    </div>
  </div>
</form>
</div>

<div class="container-fluid">
    <h2 class="text-center font-weight-bold font-italic text-primary">Lista de facturas</h2>
    </div>
    <div class="container-fluid">
    <table id="example" border="1" class="table table-hover data-table">
      <thead class="thead-dark">
        <tr>
        <th>Fecha</th>
        <th>Tipo de pago</th>
        <th>Cliente</th>
        <th>Vendedor</th>
        <th>Sucursal</th>
        <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($facturaP as $f)
        <tr>
          <td>{{ $f->fecha }}</td>
          <td>{{ $f->descripcion }}</td>
          <td>{{ $f->cliente }}</td>
          <td>{{ $f->nombre.' '.$f->apellido }}</td>
          <td>{{ $f->nombreSucursal }}</td>
          <td> <a href="Detalle" class="btn btn-primary"role="button" aria-pressed="true">Ver detalles</a>
            <a href="{{Route('eliminarF', $f->idFactura)}}" class="btn btn-danger" role="button" aria-pressed="true">Eliminar</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    </div>
    <!-- Scripts -->
      <script src="https://code.jquery.com/jquery-3.3.1.js"></script> 
      
      <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>  
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );  
</script>
	</body>
</html>