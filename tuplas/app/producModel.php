<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producModel extends Model
{
  	protected $table = 'producto';
  	protected $primaryKey = 'id_producto';
}
