<?php 
use Illuminate\Database\Seeder;
use App\cliente;
 
 class clienteSeeder extends Seeder
 {
 	
 	public function run(){
 		\DB::table('cliente')->insert([
			"nombre" => "Rick",
			"apellido" => "Grimmes",
			"direccion" => "Alexandria",
			"fecha_nacimiento" => "1986-04-12",
			"telefono" => "2145-2544",
			"email" => "rick@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Tara",
			"apellido" => "Ramirez",
			"direccion" => "Hiltop",
			"fecha_nacimiento" => "1990-01-22",
			"telefono" => "2345-7417",
			"email" => "tara@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Judith",
			"apellido" => "Grimmes",
			"direccion" => "Alexandria",
			"fecha_nacimiento" => "2000-4-23",
			"telefono" => "2145-2544",
			"email" => "judith@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Carol",
			"apellido" => "Martinez",
			"direccion" => "Soyapango",
			"fecha_nacimiento" => "1975-03-12",
			"telefono" => "7812-4512",
			"email" => "carol@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "pedro",
			"apellido" => "Mendoza",
			"direccion" => "Ciudad delgado",
			"fecha_nacimiento" => "1993-07-12",
			"telefono" => "2112-4745",
			"email" => "pedrux@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Edgardo",
			"apellido" => "Salinas",
			"direccion" => "San Salvador",
			"fecha_nacimiento" => "1994-05-02",
			"telefono" => "7845-1412",
			"email" => "edgardo@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Rebeca",
			"apellido" => "Iraheta",
			"direccion" => "San Marcos",
			"fecha_nacimiento" => "1995-10-23",
			"telefono" => "6167-7017",
			"email" => "rbk@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Elizabeth",
			"apellido" => "Castellanos",
			"direccion" => "San Martin",
			"fecha_nacimiento" => "1997-09-11",
			"telefono" => "7121-9761",
			"email" => "elizabeth@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Gabriela",
			"apellido" => "Aguilar",
			"direccion" => "Soyapango",
			"fecha_nacimiento" => "1997-04-12",
			"telefono" => "7541-1245",
			"email" => "gaby@gmail.com"
		]);

		\DB::table('cliente')->insert([
			"nombre" => "Sarahi",
			"apellido" => "Hernadez",
			"direccion" => "Quezaltepeque",
			"fecha_nacimiento" => "1998-05-07",
			"telefono" => "2348-7445",
			"email" => "sarahi@gmail.com"
		]);
 	}
 }