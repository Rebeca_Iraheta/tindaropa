<?php 
use Illuminate\Database\Seeder;
use App\factura;

class facturaSeeder extends Seeder
{
	public function run(){
			\DB::table('factura')->insert([
			"num_factura" => "001",
			"codigo" => "001",
			"id_cliente" => "1",
			"fecha" => "2020-02-27",
			"num_pago" => "1"				
		]);

			\DB::table('factura')->insert([
			"num_factura" => "002",
			"codigo" => "002",
			"id_cliente" => "2",
			"fecha" => "2020-02-27",
			"num_pago" => "2"			
		]);

			\DB::table('factura')->insert([
			"num_factura" => "003",
			"codigo" => "003",
			"id_cliente" => "1",
			"fecha" => "2020-02-27",
			"num_pago" => "1"				
		]);

			\DB::table('factura')->insert([
			"num_factura" => "004",
			"codigo" => "004",
			"id_cliente" => "2",
			"fecha" => "2020-02-27",
			"num_pago" => "2"				
		]);

			\DB::table('factura')->insert([
			"num_factura" => "005",
			"codigo" => "005",
			"id_cliente" => "3",
			"fecha" => "2020-02-27",
			"num_pago" => "1"				
		]);

			\DB::table('factura')->insert([
			"num_factura" => "006",
			"codigo" => "006",
			"id_cliente" => "2",
			"fecha" => "2020-02-27",
			"num_pago" => "2"			
		]);

			\DB::table('factura')->insert([
			"num_factura" => "007",
			"codigo" => "007",
			"id_cliente" => "1",
			"fecha" => "2020-02-27",
			"num_pago" => "1"				
		]);

			\DB::table('factura')->insert([
			"num_factura" => "008",
			"codigo" => "008",
			"id_cliente" => "2",
			"fecha" => "2020-02-27",
			"num_pago" => "2"				
		]);

			\DB::table('factura')->insert([
			"num_factura" => "009",
			"codigo" => "009",
			"id_cliente" => "3",
			"fecha" => "2020-02-27",
			"num_pago" => "1"			
		]);

			\DB::table('factura')->insert([
			"num_factura" => "010",
			"codigo" => "010",
			"id_cliente" => "4",
			"fecha" => "2020-02-27",
			"num_pago" => "2"				
		]);

	}
}