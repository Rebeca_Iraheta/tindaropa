<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(categoria::class);
        //$this->call(clienteSeeder::class);
        //$this->call(detalleSeeder::class);
        //$this->call(facturaSeeder::class);
        //$this->call(modo_pagoSeeder::class);
        $this->call(productoSeeder::class);
    }
}
