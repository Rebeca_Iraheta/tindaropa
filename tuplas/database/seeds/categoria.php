<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;


class categoria extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categoria')->insert([
			"nombre" => "Bebidas",
			"descripcion" => "gaseosas y mas"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Granos basicos",
			"descripcion" => "Lo necesario para sus alimentos"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Embutidos",
			"descripcion" => "siempre frescos"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Juguetes",
			"descripcion" => "para el entretenimiento"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Alimentos de mascotas",
			"descripcion" => "el alimento ideal de su mascota"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Carnes",
			"descripcion" => "carne importada y fresca"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Ropa",
			"descripcion" => "Siempre a su talla"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Electronico",
			"descripcion" => "telefonos, audifonos y mas"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Perfumes",
			"descripcion" => "variedad de perfumes"
		]);

		\DB::table('categoria')->insert([
			"nombre" => "Maquillaje",
			"descripcion" => "todo lo que ella necesita para verse mejor"
		]);
    }
}
