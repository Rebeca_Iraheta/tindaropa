<?php 
use Illuminate\Database\Seeder;
use App\modo_pago;

class modo_pagoSeeder extends Seeder
{
	
	public function run(){
		\DB::table('modo_pago')->insert([
			"num_pago" => "3",
			"nombre" => "Efectivo",
			"otros_detalles" => "pago en monedas"
		]);

		\DB::table('modo_pago')->insert([
			"num_pago" => "4",
			"nombre" => "tarjeta de credito",
			"otros_detalles" => "pago de tarjeta"
		]);

		\DB::table('modo_pago')->insert([
			"num_pago" => "5",
			"nombre" => "tarjeta de regalo",
			"otros_detalles" => "pago con tarjeta"
		]);

		\DB::table('modo_pago')->insert([
			"num_pago" => "6",
			"nombre" => "cheque",
			"otros_detalles" => "aplica para empresas"
		]);
	}
}