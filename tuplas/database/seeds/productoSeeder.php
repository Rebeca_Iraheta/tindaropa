<?php 
use Illuminate\Database\Seeder;
use App\producModel;

class productoSeeder extends Seeder
{
	
	public function run(){
		\DB::table('producto')->insert([
			"nombre" => "Servilletas",
			"precio" => "0.25",
			"stock" => "100",
			"id_categoria" => "1"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Jabon de ropa",
			"precio" => "0.60",
			"stock" => "120",
			"id_categoria" => "1"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Aceite",
			"precio" => "1.25",
			"stock" => "19",
			"id_categoria" => "2"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Canela molida",
			"precio" => "0.25",
			"stock" => "100",
			"id_categoria" => "3"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Atun",
			"precio" => "0.75",
			"stock" => "50",
			"id_categoria" => "2"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Papel Toalla",
			"precio" => "1.00",
			"stock" => "125",
			"id_categoria" => "1"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Papel aluminio",
			"precio" => "0.75",
			"stock" => "75",
			"id_categoria" => "1"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Consome",
			"precio" => "0.25",
			"stock" => "25",
			"id_categoria" => "3"
		]);

		\DB::table('producto')->insert([
			"nombre" => "Margarina",
			"precio" => "0.80",
			"stock" => "100",
			"id_categoria" => "2"
		]);

		\DB::table('producto')->insert([
			"nombre" => "jabon de olor",
			"precio" => "0.50",
			"stock" => "100",
			"id_categoria" => "1"
		]);
	}
}