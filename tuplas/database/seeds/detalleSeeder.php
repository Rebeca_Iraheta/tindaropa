<?php 
use Illuminate\Database\Seeder;
use App\detalle;

class detalleSeeder extends Seeder
{
	
	public function run(){
		\DB::table('detalle')->insert([
			"num_factura" => "001",
			"id_producto" => "1",
			"cantidad" => "2",
			"precio" => "0.50"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "002",
			"id_producto" => "2",
			"cantidad" => "1",
			"precio" => "1.25"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "003",
			"id_producto" => "1",
			"cantidad" => "5",
			"precio" => "5"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "004",
			"id_producto" => "2",
			"cantidad" => "3",
			"precio" => "3.25"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "005",
			"id_producto" => "3",
			"cantidad" => "4",
			"precio" => "1.50"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "006",
			"id_producto" => "1",
			"cantidad" => "4",
			"precio" => "4"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "007",
			"id_producto" => "2",
			"cantidad" => "4",
			"precio" => "2.50"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "008",
			"id_producto" => "3",
			"cantidad" => "8",
			"precio" => "5.00"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "009",
			"id_producto" => "4",
			"cantidad" => "1",
			"precio" => "5.00"
		]);

		\DB::table('detalle')->insert([
			"num_factura" => "010",
			"id_producto" => "1",
			"cantidad" => "2",
			"precio" => "2"
		]);
	}
}